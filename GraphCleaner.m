classdef GraphCleaner < handle
    %DATATRACE Data to be matched
    
    properties
        G % Graph
        newG %Cleaned Graph
        
        nodeList %List of visited nodes
        nodeData % Paths to nodes
        approvedEdges; %paths to start node
        
        EdgeVariables
    end
    
    methods      
        function obj = GraphCleaner(Graph)
            obj.G = Graph;
            obj.SetVariableNames();
            obj.clearVariables();
        end
        
        function CleanGraph(obj,depth,threshold)
            if nargin<2
                depth = 2;
            end
            if nargin<3
                threshold = 2;
            end
            assert(depth>1,'depth needs to be bigger than 1')
            
            %Clear old data
            obj.approvedEdges = [];
            obj.clearVariables();
            
            %Check nodes
            obj.checkNodes(depth,threshold)
            
            %Create new graph
            obj.GenerateNewGraph;
        end
        
        function clearVariables(obj)
            obj.nodeList = [];
            obj.nodeData = struct('branch',[],'edges',[],'discovered',0);
        end
        
        function SetVariableNames(obj)
            obj.EdgeVariables = obj.G.Edges.Properties.VariableNames;
        end
        
        function newG = GenerateNewGraph(obj)
            newG = digraph();
            newG = addnode(newG,obj.G.Nodes);
            
            h = waitbar(0,'Generating new Graph');
            for ii = 1:size(obj.approvedEdges,1)
                in = num2str(obj.approvedEdges(ii,1));
                out = num2str(obj.approvedEdges(ii,2));
                
                %weight = obj.approvedEdges(ii,3:end);
                properties = num2cell(obj.approvedEdges(ii,3:end));
                %obj.G.Edges(findedge(obj.G,in,out),:).Weight;
                newG = addedge(newG,table({in,out},properties{:}, ...
                    'VariableNames', obj.EdgeVariables));
                waitbar(ii/size(obj.approvedEdges,1),h)                
            end
            close(h);
            obj.newG = newG;
        end
        
        function checkNodes(obj,depth,threshold)
            h=waitbar(0,'Checking paths');
            for ii = 1:size(obj.G.Nodes,1)
                startNode = str2double(obj.G.Nodes(ii,:).Name{1});
                edgesidx = outedges(obj.G,ii);
                if numel(edgesidx)>1
                    obj.branchSearch(edgesidx,depth);
                    obj.CheckEdges(startNode,threshold)
                end
                obj.clearVariables();
                waitbar(ii/size(obj.G.Nodes,1),h)
            end
            close(h)
        end
            
        function branchSearch(obj,edgesIdx,depth)
            for ii = 1:numel(edgesIdx)
                eid = edgesIdx(ii);
                thisnode = str2double(obj.G.Edges(eid,:).EndNodes{1});
                newnode = str2double(obj.G.Edges(eid,:).EndNodes{2});
                %weight = obj.G.Edges(eid,:).Weight;
                %obj.depthsearch(newnode, {newnode}, thisnode, depth-1, weight)
                properties=[];
                for pp = 2 :numel(obj.EdgeVariables)
                    eval(['properties(end+1)=obj.G.Edges(eid,:).' obj.EdgeVariables{pp} ';'])
                end

                obj.depthsearch(newnode, newnode, thisnode, depth-1, properties)
            end
        end
                
        function depthsearch(obj, node, branch, oldnode, depth, properties)
            %node_ = str2double(node);
            %branch_ = str2double(branch);
            %branch_=cellfun(@str2double, branch);
            %oldnode_ = str2double(oldnode);
            
            if any(node==obj.nodeList)
                nodeid = find(node==obj.nodeList);
                idx = numel(obj.nodeData(nodeid).branch)+1;
                disc = obj.nodeData(nodeid).discovered+1;
            else
                nodeid = numel(obj.nodeList)+1;
                obj.nodeList(nodeid) = node;
                idx = 1;
                disc = 1;
            end
            
            if nodeid>numel(obj.nodeData) || ~any(branch'==obj.nodeData(nodeid).branch,'all')
                obj.nodeData(nodeid).branch(idx:idx+numel(branch)-1)=branch;
                obj.nodeData(nodeid).edges(idx,:) = [oldnode, node, properties];
                branch = obj.nodeData(nodeid).branch;
                obj.nodeData(nodeid).discovered = disc;
            end
            
            if depth == 0
                return;
            elseif disc==1
                eid = outedges(obj.G,num2str(node));
                for ii=1:numel(eid)
                    newnode = str2double(obj.G.Edges(eid(ii),:).EndNodes{2});
                    properties=[];
                    %weight = obj.G.Edges(eid(ii),:).Weight;
                    for pp = 2 :numel(obj.EdgeVariables)
                        eval(['properties(end+1)=obj.G.Edges(eid(ii),:).' obj.EdgeVariables{pp} ';'])
                    end
                    obj.depthsearch(newnode, branch, node, depth-1, properties);
                end
            end
        end
        
        function CheckEdges(obj,startNode,threshold)
            assert(numel(obj.nodeList)==numel(obj.nodeData))
            
            for ii = numel(obj.nodeList):-1:1
                if obj.nodeData(ii).discovered>=threshold
                %if numel(obj.nodeData(ii).branch)>=threshold
                    obj.AddEdges(ii,startNode)
                end
            end
        end
        
        function AddEdges(obj, nodeid, startNode)
            %for ii = 1:numel(obj.nodeData(nodeid).branch)
            for ii = 1:size(obj.nodeData(nodeid).edges,1)    
                edges_ = obj.nodeData(nodeid).edges(ii,:);
                
                %Check if edges are already added
                if isempty(obj.approvedEdges) || ...
                        ~any(sum(obj.approvedEdges(:,1:2)==edges_(1:2),2)==2) 
                    obj.approvedEdges(end+1,:) = edges_;
                end

                back_node=edges_(1);
                if back_node~=startNode && ...
                    any(back_node==obj.nodeList)
                    newnodeid = find(back_node==obj.nodeList);
                    obj.AddEdges(newnodeid,startNode) %#ok<FNDSB>
                end

            end
        end
        

            
    end
    
    
end