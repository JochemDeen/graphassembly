classdef GraphPathFinder < handle
    %DATATRACE Data to be matched
    
    properties
        G % Graph
        Traces %Traces of the graph
        
        bins % list of bins per node
        components %Componens and their frequency
        
        reachedBefore %list if node is reached before
        nodeScores %Maximal lag per score
        bestprevNodes %where does it come from
        
        nodeDepths %Depth connected to skeleton nodes
        nodeLags %Lags for all nodes in consensus trace
        bestMaster %Best Master node for aligning consensus
    end
    
    methods      
        function obj = GraphPathFinder(Graph, traces)
            obj.G = Graph;
            obj.Traces = traces;
            
            obj.setbins();
        end
        
        function setbins(obj)
            obj.bins = conncomp(obj.G,'Type','weak');
            vals=unique(obj.bins);
            
            amp=zeros(numel(vals,1));
            for ii=1:numel(vals)
                amp(ii) = sum(obj.bins==vals(ii));
            end
            obj.components=sortrows(horzcat(vals(:),amp(:)),2,'descend');            
        end
        
        function idc = getNodes(obj,comp)
            idc = find(obj.bins==comp);
        end
        
        function terminalNodes = FindTerminalNodes(obj, nodes) %% Start nodes
            terminalNodes=[];
            for node_i = nodes
                if numel(inedges(obj.G,node_i))==0
                    terminalNodes(end+1) = node_i;
                end
            end
        end
        
        function nr_nodes = GetNrNodes(obj)
            nr_nodes = size(obj.G.Nodes,1);
        end
        
        function ClearVariables(obj,nr_nodes)
            obj.node_colors = ones(nr_nodes,1) * -1; %Or length of all? %%TODO
        end
        
        function lag = GetLag(obj,eid)
            lag = obj.G.Edges(eid,:).Weight;
        end
        
        function length = GetLength(obj, nodeid)
            mapid = str2double(obj.G.Nodes(nodeid,:).Name);
            length = numel(obj.Traces{mapid});
            %length = 375; %%TODO fix later
        end
        
        function setInitialVariables(obj,nr_nodes)
            if nargin<2
                nr_nodes = obj.GetNrNodes();
            end
            obj.reachedBefore = ones(nr_nodes,1) * -1;
            obj.nodeScores = ones(nr_nodes,1) * -1;
            obj.bestprevNodes = ones(nr_nodes,1) * -2;
        end
        
        function SetInitialVariablesConsensus(obj,nr_nodes)
            if nargin<2
                nr_nodes = obj.GetNrNodes();
            end
            obj.setInitialVariables(nr_nodes);
            
            obj.nodeDepths = ones(nr_nodes,1) * -1;
            obj.bestMaster = ones(nr_nodes,1) * -1;
            obj.nodeLags = ones(nr_nodes,1) * -1;
        end
                
        
        function [path, topScore] = FindLongestPath(obj,comp)
            nodes = obj.getNodes(comp);
            
            terminalNodes = obj.FindTerminalNodes(nodes);
            obj.setInitialVariables(obj.GetNrNodes())
            
            for node_i = terminalNodes
               obj.linear_path_dfs(node_i, -1, 0,node_i) 
            end
            
            topScore = 0;
            for node_i = nodes
               if  obj.nodeScores(node_i)>0
                  termLength = obj.GetLength(node_i);
                  if obj.nodeScores(node_i)+termLength>topScore
                      topScore = obj.nodeScores(node_i)+termLength;
                      endNode = node_i;
                  end
               end
            end
            disp(topScore)
            
            curNode = endNode;
            extentionComplete = false;
            path =[];
            while ~extentionComplete
                path(end+1) = curNode;
                if obj.bestprevNodes(curNode)>0
                    curNode = obj.bestprevNodes(curNode);
                else
                    extentionComplete = true;
                end
            end 
            
            path = flip(path);
        end
        
        function linear_path_dfs(obj,node, prevNode, cur_score,master_node)
            if obj.reachedBefore(node) == master_node
                return %node already visited
            else
                obj.reachedBefore(node) = master_node;
                if obj.nodeScores(node) < cur_score
                    obj.nodeScores(node) = cur_score;
                    obj.bestprevNodes(node) = prevNode;
                    
                    eid = outedges(obj.G,node);
                    for ii =1 : numel(eid)
                        newnode = findnode(obj.G,obj.G.Edges(eid(ii),:).EndNodes{2});                        
                        %lag = obj.GetLag(obj.G.Edges(eid(ii),:).alignment);
                        lag = obj.GetLag(eid(ii));
                        
                        obj.linear_path_dfs(newnode,node,cur_score + lag, master_node);
                    end
                end
                return;
            end
        end
        
        function cons = CreateConsensus(obj,path,topScore)
            
            %Generate lags for all the nodes in the path relative to start:
            lags = zeros(numel(path),1);
            for ii = 2:numel(path)
                node_i = path(ii-1);
                node_j = path(ii);
                
                eid = findedge(obj.G,node_i,node_j);
                lags(ii) = obj.G.Edges(eid,:).Weight;
                %lags(ii) = lag;
            end
            
            skeleton = cumsum(lags);
            
            %Initialize variables
            obj.SetInitialVariablesConsensus();

            %Set lags for nodes in path:
            %obj.lagsNodes = ones(size(obj.G.Nodes,1)) * -1;
            obj.nodeLags(path) = skeleton;
            obj.nodeDepths(path) = 0;
            
            disp('Aligning Nodes to Skeleton')
            h=waitbar(0,'Aligning Nodes to Skeleton');
            for node_i = path
               thisLag = obj.nodeLags(node_i);
               maxLag = obj.nodeLags(node_i)+obj.GetLength(node_i);

                eid = outedges(obj.G,node_i);
                for ii =1 : numel(eid)
                    newnode = findnode(obj.G,obj.G.Edges(eid(ii),:).EndNodes{2});
                    lag_ = obj.GetLag(eid(ii));

                    obj.distance_to_nodes_dfs(newnode, 1, thisLag + lag_, node_i, maxLag);
                end

                % obj.distance_to_nodes_dfs(node_i, 0, thisLag, node_i, maxLag);
               
               waitbar(find(node_i==path)/numel(path),h)
            end
            close(h)
            
            %nodesSorted = find(obj.nodeDepths>-1)
            %Sort Lags
            [nodeLagsSorted, nodesI] = sort(obj.nodeLags);
            nodesI = nodesI(nodeLagsSorted>=0);
            nodeLagsSorted = nodeLagsSorted(nodeLagsSorted>=0);
            
            disp('Generating consensus')
            cons = obj.CreateConsensusTraceSimple(nodesI,nodeLagsSorted,topScore);
            
        end
        
        function distance_to_nodes_dfs(obj, node, depth, lag, master_node, maxLag)
            if obj.reachedBefore(node) == master_node || lag>maxLag
                return %node already visited
            else
                obj.reachedBefore(node) = master_node;
                
                %if distance to master node is less than previously found
                if obj.nodeDepths(node)==-1 || depth < obj.nodeDepths(node)
                    obj.nodeDepths(node) = depth;
                    obj.bestMaster(node) = master_node;
                    obj.nodeLags(node) = lag;
                    
                    
                    eid = outedges(obj.G,node);
                    for ii =1 : numel(eid)
                        newnode = findnode(obj.G,obj.G.Edges(eid(ii),:).EndNodes{2});
                        lag_ = obj.GetLag(eid(ii));
                        
                        
                        obj.distance_to_nodes_dfs(newnode,depth + 1, lag + lag_, master_node, maxLag);
                    end
                end
                return;
            end
        end
        
        function cons = CreateConsensusTraceSimple(obj,nodeidS,lags,topScore)
            %Initialize consensus
            cons=nan(numel(nodeidS),ceil(topScore));
            
            for ii = 1:numel(nodeidS)
                node_id = nodeidS(ii);
                mapid = str2double(obj.G.Nodes(node_id,:).Name);
                direction = obj.G.Nodes(node_id,:).Direction;
                lag = lags(ii);
                
                if direction == 1
                    map = obj.Traces{mapid};
                elseif direction == 2
                    map = flip(obj.Traces{mapid});
                end
                
                cons(ii,lag+1:lag+numel(map)) = map;
                
            end
        end
        
    end

end