function [maxscore,normalizedScore,direction,lag,trace]=SWIMatchingSimple(datatrace1,datatrace2,varargin)
datatrace1=datatrace1(:);

sigma = .5;
penalty = -1.7;
normalize = false;
lengthnormalize = false;
minoverlap = 200;

for ii = 1:length(varargin)
    if ischar(varargin{ii})
        switch upper(varargin{ii})
            case 'SIGMA'
                sigma = varargin{ii+1};
            case 'PENALTY' 
                penalty = varargin{ii+1};
            case 'NORMALIZE'
                normalize = true;
            case 'LENGTHNORMALIZE'
                lengthnormalize = true;
            case 'MINOVERLAP'
                minoverlap = varargin{ii+1};
        end
    end
end

if normalize
    datatrace1 = datatrace1 - mean(datatrace1);
    datatrace1 = datatrace1./ std(datatrace1);
    datatrace2 = datatrace2 - mean(datatrace2);
    datatrace2 = datatrace2./ std(datatrace2);
end

minoverlap = min([minoverlap, numel(datatrace1)-1,numel(datatrace2)-1]);


SM=makeScoreMatrix(datatrace1(:),datatrace2(:));

if lengthnormalize
    %Direction1
    [~ ,~ ,Scoring_Matrix,fromi,fromj] = fastfitter(SM,penalty);
    
    %[trace, maxscore] = AdjustScoresByEnergy(Scoring_Matrix,minoverlap, datatrace1(:),datatrace2(:), fromi, fromj, trace(1,1)>trace(end,1));
    [trace1, maxscore1] = AdjustScores(Scoring_Matrix,minoverlap, fromi,fromj);
    
    datatrace1_=datatrace1(trace1(:,1));
    datatrace2_=datatrace2(trace1(:,2));
    norm = sqrt(sum(datatrace1_.^2)*sum(datatrace2_.^2));
    normalizedScore1 = maxscore1/norm;
    
    %Direction2
    [~, ~,Scoring_Matrix,fromi,fromj] = fastfitter(flipud(SM),penalty);
    
    [trace2, maxscore2] = AdjustScores(Scoring_Matrix,minoverlap, fromi,fromj);
    trace2(:,1)=numel(datatrace1)-trace2(:,1)+1;

    datatrace1_=datatrace1(trace2(:,1));
    datatrace2_=datatrace2(trace2(:,2));
    norm = sqrt(sum(datatrace1_.^2)*sum(datatrace2_.^2));
    normalizedScore2 = maxscore2/norm;
    
    if normalizedScore1>=normalizedScore2
        normalizedScore = normalizedScore1;
        maxscore = maxscore1;
        trace=trace1;
    else
        normalizedScore = normalizedScore2;
        maxscore = maxscore2;
        trace=trace2;
    end
else
    [maxscore, trace,~ ] = fastfitter(SM,penalty);

    datatrace1_=datatrace1(trace(:,1));
    datatrace2_=datatrace2(trace(:,2));
    norm = sqrt(sum(datatrace1_.^2)*sum(datatrace2_.^2));
    normalizedScore = maxscore/norm;
end

%Determine lag of 2nd trace relative to the 1st:
%pixels of datatrace1 dangling on left of 2nd trace
 [lag, direction] = FindLags(trace,numel(datatrace1));