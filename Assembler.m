classdef Assembler < handle
  %function G=Assembler(traces)
  % This function tries to combine multiple map into an assembled map

  properties
    %Variables

    startTraces; %Defined at start
    traces; %to be used for everything else
    calibratedTraces;
    scoreMatrix; %Cross correlation score matrix
    alignments; %Pair-wise alignments
    i_idx;
    j_idx;

    G; %Assembly graph
    graphCleaner; %Structure for cleaning the graph
    rejected; %List of rejected alignments
    idxinGraph; %index of alignment in graph
    graphPathFinder; %Structure for finding the longest path

    %Plotting
    verbose;
    plotson;

    %Parameters
    minOverlap; %minimal overlap for cross correlation and alignment
    xcorrT; %Cross correlation threshold for calculating a SW alignment
    swT; %Smith-Waterman threshold for adding Traces to the Graph
    depth; %depth for dfs in cleaning the graph
    minPaths; %minimal number of paths must link two nodes
    penalty; %Penalty for NW alignment
    length; 

    std_val; %Standard deviation for variation
    norm_px; %normalization pixels

  end

  methods
      
      function obj = Assembler(traces,verbose,plotson)
          if size(traces,2)>size(traces,1)
              traces=traces';
          end
          obj.startTraces = traces;
          obj.traces = traces;
          
          if ~exist('verbose','var') || isempty(verbose)
              obj.verbose = true;
          else
              obj.verbose = verbose;
          end
          if ~exist('plotson','var') || isempty(plotson)
              obj.plotson = true;
          else
              obj.plotson = plotson;
          end
          obj.Initialize();
      end
      
      function Initialize(obj)
          obj.minOverlap = []; %minimal overlap for cross correlation and alignment
          obj.xcorrT = 0; %Cross correlation threshold for calculating a SW alignment
          obj.swT = 0; %Smith-Waterman threshold for adding Traces to the Graph
          obj.length = -1;
          obj.depth = 0; %depth for dfs in cleaning the graph
          obj.minPaths = 0; %minimal number of paths must link two nodes
          obj.penalty = []; %Penalty for NW alignment
          
          obj.std_val = []; %Standard deviation for variation
          obj.norm_px = []; %normalization pixels
      end
      
      function AdjustTraces(obj,std_val,norm_px)
          if ~exist('std_val','var') || isempty(std_val)
              std_val = 5;
          end
          if ~exist('norm_px','var') || isempty(norm_px)
              norm_px = 100;
          end
          
          if std_val~=obj.std_val
              obj.FilterTraces(std_val)
          end
          
          if norm_px~=obj.norm_px
              obj.LocalNormalization(norm_px)
          end
          
          obj.CalibrateTraces();
      end
           
      % Filter some traces
      function FilterTraces(obj,std_val)
          if ~isequal(std_val,obj.std_val)
              traces_=obj.startTraces;
              
              t=cell2mat(traces_');
              t=t(:);
              maxV = mean(t)+std_val*std(t);
              
              for ii=numel(traces_):-1:1
                  if any(traces_{ii}>maxV)
                      traces_(ii)=[];
                  end
              end
              if obj.verbose;disp([' Removed: ' num2str(numel(obj.startTraces)-numel(traces_)) ' Traces' ]);end
              obj.traces = traces_;
              obj.std_val = std_val;
          end
      end
      
      % Filter some traces
      function FilterTracesByLength(obj,length)
          if ~isequal(length,obj.length)
              traces_=obj.startTraces;
              traces_(cellfun('length',traces_)<length)=[];
              if obj.verbose;disp(['Removed ' num2str(numel(obj.traces)-numel(traces_)) ' traces']);end
              obj.traces = traces_;
              obj.length = length;
          end
      end
      
      %Local normalization
      function LocalNormalization(obj,norm_px)
          if ~isequal(norm_px,obj.norm_px)
              normTraces={};
              for ii=1:numel(obj.traces)
                  normTraces{ii,1}=center_normalize_local(obj.traces{ii},norm_px);
              end
              obj.traces = normTraces;
              obj.norm_px = norm_px;
              if obj.verbose;disp(' Normalization done' );end
          end
      end
      
      % Calibrate Traces to unity
      function CalibrateTraces(obj)
          %Calibrate traces
          obj.calibratedTraces = CalibrateTraces(obj.traces);
          if obj.verbose;disp('Calibration done' );end
      end
      
      % Calculate pair-wise overlap crossCorrelation matrix
      function OverlapCrossCorrelation(obj,minOverlap)
          if ~exist('minOverlap','var') || isempty(minOverlap)
              minOverlap = 5;
          end
          if ~isequal(minOverlap,obj.minOverlap)
              
              scoreMatrix = OverlapCrossCorrelation(obj.traces, minOverlap, obj.verbose);
              
              obj.scoreMatrix = scoreMatrix;
              obj.minOverlap = minOverlap;
          end
          if obj.plotson
              figure(699);
              histogram(obj.scoreMatrix(obj.scoreMatrix>0))
              title(['histogram Overlap cross correlation with overlap: ' num2str(obj.minOverlap)])
          end
      end
      
      % Calculate pair-wise Smith Waterman Alignments For pairs with high cross correlation
      function PairWiseAlignment(obj,xcorrT,penalty,minOverlap)
          if ~exist('xcorrT','var') || isempty(xcorrT)
              xcorrT = 0.5;
          end
          if ~exist('penalty','var') || isempty(penalty)
              penalty = -2;
          end
          if ~exist('minOverlap','var') || isempty(minOverlap)
              minOverlap = 200;
          end
          
          
          if ~isequal(xcorrT,obj.xcorrT) || ~isequal(penalty,obj.penalty) || ~isequal(minOverlap,obj.minOverlap)
              %Sort all scores of ScoreMatrix
              M2=obj.scoreMatrix(:);
              [vals,idcs] = sort(M2,'descend');
              
              merge = xcorrT<obj.xcorrT && isequal(penalty,obj.penalty) && isequal(minOverlap,obj.minOverlap);
              if merge
                  maxval = obj.xcorrT;
              else
                  maxval  = max(vals);
              end
              
              %Select scores higher than xcorrT
              idcs_=idcs(vals>xcorrT & vals<=maxval);
              
              %Get map_i and map_j values
              [i_idx,j_idx] = ind2sub(size(obj.scoreMatrix),idcs_);
              
              if numel(i_idx)>0
                  alignments = OverlapSWAlignment(obj.traces, [i_idx,j_idx], penalty, minOverlap, obj.verbose);

                  if merge
                      obj.alignments = MergeStructs(obj.alignments,alignments);
                  else
                      obj.alignments = alignments;
                  end
              end
              
              obj.minOverlap = minOverlap;
              obj.penalty = penalty;
              obj.xcorrT = xcorrT;
          end
          if obj.plotson
              figure(679)
              histogram([obj.alignments.normalizedScores]);
              title('histogram Alignment scores')
          end
      end
      
      %% Create Assembly Graph
      function CreateAssemblyGraph(obj,swT)
          if ~exist('swT','var') || isempty(swT)
              swT = 0.5;
          end
          %assert(swT>obj.xcorrT);
          
          if ~isequal(swT,obj.swT)
              
              %initialize values
              G=digraph(); %Final Graph
              idxinGraph=[]; %indices of Traces in Graph
              rejected=[]; %List of pairs that are rejected
              
              %Sort alignment scores
              [scores,idxS] = sort([obj.alignments.normalizedScores],'descend');
              
              %Filter scores that have poor overlap
              scores(abs([obj.alignments(idxS).Overlap])<obj.minOverlap)= 0;
              
              if swT<obj.swT
                  maxScore = obj.swT;
                  G = obj.G;
              else
                  maxScore = max(scores);
              end
              
              %Select scores that match the threshold
              approvedScores = scores(scores>swT & scores<=maxScore);
              %Ordered list of alignment pairs
              idxS = idxS(scores>swT & scores<=maxScore);
              
              h=waitbar(0,'Adding maps to the graph');
              for ii = 1 :numel(approvedScores)
                  waitbar(ii/numel(approvedScores),h)
                  
                  %get mapindices, directions and lag of pairwise alignment
                  [idx1,idx2,d1,d2,centerDistance] = GetMaps(obj.alignments(idxS(ii)));
                  
                  %Attempt to add to graph
                  [G,idxinGraph, rejected ]= AddToGraph(G,idx1,idx2, d1,d2,centerDistance,idxinGraph,rejected,idxS(ii),obj.verbose);
                  
                  if obj.plotson
                      figure(678);cla
                      plot(G,'layout','auto');
                      pause(0.001);
                  end
              end
              close(h);
              
              if obj.verbose;disp('Finished 1st Graph Assembly');end
              obj.G=G;
              obj.swT = swT;
              obj.rejected = rejected;
              obj.idxinGraph = idxinGraph;
          end
      end
      
      % Clean graph
      
      function CleanGraph(obj,depth,minPaths)
          if ~exist('depth','var') || isempty(depth)
              depth = 3;
          end
          
          if ~exist('minPaths','var') || isempty(minPaths)
              minPaths = 2;
          end
          
          if ~isequal(depth,obj.depth) || ~isequal(minPaths,obj.minPaths)
              if obj.verbose;disp('Cleaning Graph');end
              
              graphCleaner = GraphCleaner(obj.G); %#ok<*PROPLC>
              graphCleaner.CleanGraph(depth,minPaths);
              G = graphCleaner.newG;
              
              if obj.verbose;disp('Cleaning done');end
              if obj.plotson
                  figure(678);cla
                  hp=plot(G,'layout','auto');
                  pause(0.001);
              end
              
              obj.graphCleaner = graphCleaner;
              obj.G = G;
              obj.minPaths = minPaths;
              obj.depth = depth;
          end
      end
      
      function AddRejected(obj)
          %% Try and add the rejected nodes now that the graph is cleaned
          rejected2= [];
          G = obj.G; %#ok<*PROP>
          if obj.verbose; disp(['Testing ' num2str(numel(obj.rejected)) ' rejected maps']);end
          
          h=waitbar(0,'Adding rejected maps to the graph');
          idxinGraph=obj.idxinGraph;
          for jj = 1:numel(obj.rejected)
              ii=obj.rejected(jj);
              %get mapindices, directions and lag of pairwise alignment
              [idx1,idx2,d1,d2,centerDistance] = GetMaps(obj.alignments(ii));
              
              %Attempt to add to graph
              [G,idxinGraph, rejected2 ]= AddToGraph(G,idx1,idx2, d1,d2,centerDistance,idxinGraph,rejected2,ii,obj.verbose);
              
              %Plot the results
              if obj.plotson
                  figure(678);cla
                  plot(G,'layout','auto');
                  pause(0.001);
              end
              waitbar(jj/numel(obj.rejected),h)
          end
          close(h);
          obj.G = G;
          
          if obj.verbose; disp('Finished 2nd Graph Assembly');end
        obj.idxinGraph = idxinGraph;
      end
      
      %% Find longest path through the Graph
      function FindLongestPath(obj)
          obj.graphPathFinder = GraphPathFinder(obj.G,obj.calibratedTraces);
      end
      
      function cons = GenerateConcencus(obj,comp)
          if ~exist('comp','var') || isempty(comp)
              comp = obj.graphPathFinder.components(1,1);
          end
          i_ = find(obj.graphPathFinder.components(:,1)==comp);
          
          disp(['Generating contig for ' num2str(comp) ' with ' num2str(obj.graphPathFinder.components(i_,2)) ' components'])
          
          [path, topScore] = obj.graphPathFinder.FindLongestPath(comp);
          
          cons = obj.graphPathFinder.CreateConsensus(path,topScore);
      end
  end
end
