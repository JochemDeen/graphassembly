function calibratedTraces = CalibrateTraces(traces,method)

if ~exist('method','var') || isempty(method)
    method=1;
elseif method>2 || method<1
    error('Incorrect value');
end

%Convert cell to matrix
if size(traces,1)>size(traces,2)
    alltraces = cell2mat(traces);
else
    alltraces = cell2mat(traces');
end

%% Create histogram of all peaks in the program
maxv = mean(alltraces(:))+5*std(alltraces(:));
talltraces=alltraces(:);
talltraces(talltraces>maxv)=median(talltraces);
his = histogram(findpeaks(talltraces(:)),80);
%find the peaks in histogram (1st is associated with background
%second with single fluorophore peaks
[~,vals{1}] = findpeaks(his.Values,his.BinEdges(1:end-1)+.5*his.BinWidth); 
title('histogram of peak heights in data')                                  

%Initialize variables
m = zeros(1,length(traces));
b = zeros(1,length(traces));
calibratedTraces = cell(size(traces));

%Loop over all Traces and Calibrate traces
if method==1
    for ii = 1:length(traces)
        %define m for data  1/[(single fluorophore peak)-(min value in read)]
        %back =  mean([vals{1}(1) min(traces{ii})])
        back = vals{1}(1);
        m(ii) = 1/(vals{1}(2)  - back); 

        %Define background value: average of peak and min value
        b(ii) = -back * m(ii);

        calibratedTraces{ii} = (traces{ii}*m(ii))+b(ii); % apply the calibration to the WF data
    end
elseif method==2
    for ii = 1:length(traces)
        %define m for data  1/[(single fluorophore peak)-(min value in read)]
        m(ii) = 1/(vals{1}(1)  - min(traces{ii})); 

        %Define background value: average of peak and min value
        %b(ii) = - mean([vals{1}(1), min(traces{ii})]) * m(ii);
        b(ii) = - min(traces{ii}) * m(ii);

        calibratedTraces{ii} = (traces{ii}*m(ii))+b(ii); % apply the calibration to the WF data
    end
end