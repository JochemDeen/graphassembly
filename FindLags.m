function [lag, direction] = FindLags(traceidx,sizeTrace1)
p = polyfit(double(traceidx(:,1)),double(traceidx(:,2)),1);
if p(1)<0
    direction = 2;
else
    direction = 1;
end

if direction==1
    if p(2)>0
        lag =  1 - traceidx(1,2);
    else
        lag = traceidx(1,1) - 1;
    end
elseif direction==2
    if traceidx(1,1) < sizeTrace1
        lag = sizeTrace1 - traceidx(1,1);
    elseif traceidx(1,2) >= 1
        lag = 1 - traceidx(1,2);
    else
        error('case not covered by lag')
    end
end
lag=double(lag);
    