function lag = GetLagFromCenter(centerDistance,mapSize1,mapSize2)

lag = centerDistance - mapSize2/2 + mapSize1/2;