%%Create structure
loadfilename ='example.mat';
load(loadfilename)
assembler = Assembler(traces);

savefilename = 'exampleAssembler.mat';

save(savefilename,'assembler');

%% Load Assembler
savefilename = 'exampleAssembler.mat';
load(loadfilename)

%% Save Assembler
save(savefilename,'assembler')

%% Filter Traces
std_val = 5;

assembler.FilterTraces(std_val)

%% Normalize the traces
norm_px = 100;
assembler.LocalNormalization(norm_px)

%% Calibrate the traces
assembler.CalibrateTraces();

%% Cross correlation of all traces
minOverlap = 200;
assembler.OverlapCrossCorrelation(minOverlap)

%% Pair-wise overlap alignment
xcorrT = 0.5;
penalty = -2;
minOverlap = 200;
assembler.PairWiseAlignment(xcorrT,penalty,minOverlap);

%% Create assembly graph
swT = 0.5;
assembler.CreateAssemblyGraph(swT)

%% Clean Graph
depth = 3;
minPaths = 2;
assembler.CleanGraph(depth,minPaths);

%% Add rejected
assembler.AddRejected();

%% Clean Graph
depth = 3;
minPaths = 2;
assembler.CleanGraph(depth,minPaths);

%% Find Longest path
assembler.FindLongestPath();

%% Generate Concencus
assembler.GenerateConcencus();
