function Alignments = OverlapSWAlignment(traces, idxAll, penalty, minOverlap, verbose)
%This function calculates pair-wise Smith Waterman alignments for pairs that 
%have a cross correlation scores higher than xcorrT
%
% traces : A cell containing all DNA traces
% scoreMatrix: a matrix containing all the cross correlation scores for 
% xcorrT : threshold value
% penalty : penalty value for aligning Smith-Waterman traces (default -2)
% minOverlap: minimal Overlap for Smith-Waterman (default 200)
% verbose: Boolean for display (default false)

if ~exist('verbose','var') || isempty(verbose)
    verbose = false;
end
if ~exist('penalty','var') || isempty(penalty)
    penalty = -2;
end
if ~exist('minOverlap','var') || isempty(minOverlap)
    minOverlap = 200;
end
i_idx= idxAll(:,1);
j_idx = idxAll(:,2);


%Initialize
AM = zeros(size(i_idx)); %Alignment Matrix
AMM = zeros(size(i_idx)); %Maxscore
AMO = zeros(size(i_idx)); %overlap
AMD = zeros(size(i_idx)); %direction
AML = zeros(size(i_idx)); %lag
CD = zeros(size(i_idx)); %lag

WaitMessage = parfor_wait(numel(i_idx), 'Waitbar', true);

if verbose;tic;end
parfor ii=1:numel(i_idx)
    
    i_idx_ = i_idx(ii);
    j_idx_ = j_idx(ii);
    
    if verbose
        disp([num2str(round(10000*ii/numel(i_idx))/100) '% - ' num2str(i_idx_ ) '/' num2str(j_idx_ )])
    end
    
    trace1 = traces{i_idx_};
    trace2 = traces{j_idx_};
    
    if numel(trace1)>minOverlap && numel(trace2)>minOverlap
        [maxscore,normalizedScore,direction,lag,traceidx]=SWIMatchingSimple(trace1,trace2,'normalize','penalty',penalty,'lengthnormalize','minoverlap',minOverlap);        
        centerDist = FindCenterDistance(traceidx,numel(trace1),numel(trace2));
        overlap = diff([traceidx(1,1) traceidx(end,1)])+1;
    else
        normalizedScore = 0;
        maxscore = 0;
        overlap = 0;
        direction = 0;
        lag = 0;
        centerDist = 0;
        traceidx =[];
    end
        

    AM(ii)= normalizedScore;
    AMM(ii)= maxscore;
    AMO(ii)= overlap
    AMD(ii)= direction;
    AML(ii) = lag;
    CD(ii) = centerDist;
    
    allidx{ii} = traceidx;  
    WaitMessage.Send;
end
if verbose;toc;end
WaitMessage.Destroy

for ii=1:numel(i_idx)
    Alignments(ii).normalizedScores = AM(ii);
    Alignments(ii).maxScores = AMM(ii);
    Alignments(ii).Overlap = AMO(ii);
    Alignments(ii).Direction = AMD(ii);
    Alignments(ii).Lag = AML(ii);
    Alignments(ii).traceidx = allidx(ii);
    Alignments(ii).CenterDistance = CD(ii);
    Alignments(ii).i_idx = i_idx(ii);
    Alignments(ii).j_idx = j_idx(ii);
end
