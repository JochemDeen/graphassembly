function [G,idxinGraph, rejectedAlignmentList ]= AddToGraph(G, idx1, idx2, d1, d2, centerDistance, idxinGraph, rejectedAlignmentList,alignment_idx, verbose)
%This function adds alignments to the Graph, checking if one of the
%maps are already added and what direction (1 or 2) it has.
% G: The graph to which it is added
% idx1 : index of map1
% idx2 : index of map2
% d1 : direction of map1
% d2 : direction of map2
% lag : lag of map2 relative to map1
% idxinGraph : list of maps in the graph sorted by index in the graph
% rejectedAlignmentList: list of rejected maps due to direction

if ~exist('verbose','var') || isempty(verbose)
    verbose = false;
end


revDir = [2,1];

%Both maps are already in the graph
if any(idx1==idxinGraph) && any(idx2==idxinGraph)
    
    %Get direction of idx1 and idx2
    nodeidx1 = find(idx1==idxinGraph);
    nodeidx2 = find(idx2==idxinGraph);
    
    d1InGraph = G.Nodes(nodeidx1,:).Direction; %#ok<FNDSB>
    d2InGraph = G.Nodes(nodeidx2,:).Direction; %#ok<FNDSB>

    %Check in what graph component each node is
    weak_bins = conncomp(G,'Type','weak');
    
    %Are the nodes in the same graph component?
    if weak_bins(idx1==idxinGraph) == weak_bins(idx2==idxinGraph)
        %Same graph component
        if verbose;
        disp(['both ' num2str(idx1) ' and ' num2str(idx2)  ' in same graph']);end

        %If both have same orientation, just add to graph
        if d1==d1InGraph && d2==d2InGraph
            
            G = addedge(G,table({num2str(idx1),num2str(idx2)}, centerDistance, alignment_idx,...
        'VariableNames',{'EndNodes','Weight','alignment'}));
    
        elseif d1~=d1InGraph && d2~=d2InGraph
            
            %flip indices If both have inverse orientation
            [idx2,idx1] = deal(idx1,idx2);
            G = addedge(G,table({num2str(idx1),num2str(idx2)}, centerDistance, alignment_idx,...
        'VariableNames',{'EndNodes','Weight','alignment'}));
    
        else
            
            %orientation disagreement, add alignment to rejected list
            rejectedAlignmentList(end+1)=alignment_idx;
            if verbose;disp('Direction not in agreement');end
            
        end
    else
        %Different graph component, merge graphs.
        
        if verbose
        disp(['both ' num2str(idx1) ' and ' num2str(idx2)  ' in different graph']);end
    
        %if orientation is the same just add edge
        if d1==d1InGraph && d2==d2InGraph
            
            G = addedge(G,table({num2str(idx1),num2str(idx2)}, centerDistance, alignment_idx,...
        'VariableNames',{'EndNodes','Weight','alignment'}));
    
        elseif d1~=d1InGraph && d2~=d2InGraph
            %Orientation reversed for both
            
            %flip indices
            [idx2,idx1] = deal(idx1,idx2);
            G = addedge(G,table({num2str(idx1),num2str(idx2)}, centerDistance, alignment_idx,...
        'VariableNames',{'EndNodes','Weight','alignment'}));
    
        else
            %One of the nodes has a different orientation in the graph
            %Flip one of the nodes (and the graph component it is linked to) 
            % and exchange indices.
            
            if verbose;disp('flipping idx2');end

            %Gather all nodes in the same graph as idx2
            nodesidx = find(weak_bins==weak_bins(idx2==idxinGraph));

            %Reverse direction
            %for qq = 1:numel(nodesidx)
            for nodei = nodesidx
                G.Nodes(nodei,:).Direction = revDir(G.Nodes(nodei,:).Direction);
            end

            %Flip edges of outgoing edges in connected nodes
            outedgesL=[];
            %for nodei = find((sum(idxinGraph==nodesidx',1)))
            for nodei = nodesidx
                outedges_ = outedges(G,nodei);
                outedgesL(end+1:end+numel(outedges_ ))= outedges_ ;
            end
            
            outedgesL=G.Edges(outedgesL,:);
            for nodes = outedgesL.EndNodes'
                G = flipedge(G,nodes{:});
            end

            %flip indices
            %If d1 is the reverse direction ...%TODO check
            if d1 ~= d1InGraph
                [idx2,idx1] = deal(idx1,idx2);
            end

            %Add to Graph
            G = addedge(G,table({num2str(idx1),num2str(idx2)}, centerDistance, alignment_idx,...
        'VariableNames',{'EndNodes','Weight','alignment'}));
        end
    end
    
elseif any(idx1==idxinGraph)
    %Only idx1 is in the graph, Add idx2
    if verbose; disp([num2str(idx1) ' already in graph']);end
    
    %Get direction of idx1
    nodeidx1 = find(idx1==idxinGraph);
    d1InGraph = G.Nodes(nodeidx1,:).Direction; %#ok<FNDSB>

    %Add new index for idx2
    idxinGraph(end+1) = idx2;
    
    %Check if idx1 has identical orientation, otherwise flip idx2
    if d1~=d1InGraph
        [idx2,idx1] = deal(idx1,idx2);
        d2=revDir(d2);
    end
    
    %Add to graph
    G = addedge(G, table({num2str(idx1),num2str(idx2)}, centerDistance, alignment_idx,...
        'VariableNames',{'EndNodes','Weight','alignment'}));    

    %Add Direction to Nodes
    nodeid = size(G.Nodes,1);
    G.Nodes(nodeid,:).Direction = d2;
elseif any(idx2==idxinGraph)
    %only idx2 is in the graph, add the second map
    if verbose; disp([num2str(idx2) ' already in graph']);end

    %Get direction of idx2
    nodeidx2 = find(idx2==idxinGraph);
    d1InGraph = G.Nodes(nodeidx2,:).Direction; %#ok<FNDSB>
    
    %Add new index for idx1
    idxinGraph(end+1) = idx1;


    %Check if idx2 has identical orientation, otherwise flip idx1
    if d2~=d1InGraph
        %flip indices
        [idx2,idx1] = deal(idx1,idx2);
        d1=revDir(d1);
    end
        
    %Add to graph
    G = addedge(G, table({num2str(idx1),num2str(idx2)}, centerDistance, alignment_idx,...
        'VariableNames',{'EndNodes','Weight','alignment'}));

    %Add Direction to Nodes
    nodeid = size(G.Nodes,1);
    G.Nodes(nodeid,:).Direction = d1;
else
    %%Neither map is in the graph, simply add both and add orientation
    if verbose; disp('new graph');end
    
    %Add index to list
    idxinGraph(end+1) = idx1;
    idxinGraph(end+1) = idx2;

    
    %Add to graph
    G = addedge(G, table({num2str(idx1),num2str(idx2)}, centerDistance, alignment_idx,...
        'VariableNames',{'EndNodes','Weight','alignment'}));

    %For new graph
    nodeid = size(G.Nodes,1);        
    if nodeid==2
        G.Nodes.Direction = [d1;d2];
    else
        G.Nodes(nodeid-1:nodeid,:).Direction = [d1;d2];
    end

end
end