function scoreMatrix = OverlapCrossCorrelation(traces, minOverlap, verbose)
%Calculates the overlap CrosscorrelationMatrix for traces
% Returns scoreMatrix: a matrix containing all pairwise cross correlation scores 
% Traces : a cell structure containing all the traces
% minOverlap : minimal Overlap for overlap cross-correlation (default 200)
% Verbose : boolean for display

if ~exist('verbose','var') || isempty(verbose)
    verbose = false;
end
if ~exist('minOverlap','var') || isempty(minOverlap)
    minOverlap = 200;
end

if verbose;disp('normxcorr');end

scoreMatrix = zeros(numel(traces));
for i = 1:numel(traces)
    if verbose;disp(i);end
    trace_i=traces{i};
    for j = i+1:numel(traces)
        trace_j = traces{j};
        minOverlap_ = min([numel(trace_i)-1,numel(trace_j)-1,minOverlap]);
        scoreMatrix(i,j) = max(max(oxcorr(trace_i(:),trace_j(:),minOverlap_)),...
                max(oxcorr(trace_i(:),flip(trace_j(:)),minOverlap_)));
    end
end
