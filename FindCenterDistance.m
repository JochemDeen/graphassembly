function centerDist = FindCenterDistance(traceidx,sizeTrace1,sizeTrace2)

[lag,~] = FindLags(traceidx,sizeTrace1);

center1 = sizeTrace1/2;
center2 = sizeTrace2/2 + lag;

centerDist = center2 - center1;