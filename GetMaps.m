function [idx1,idx2,d1,d2,centerDistance ] = GetMaps(alignments)
    direction = alignments.Direction;
    %lag = alignments.Lag(index);
    centerDistance = alignments.CenterDistance;
    i_idx_ = alignments.i_idx;
    j_idx_ = alignments.j_idx;

    if centerDistance>=0
        d1 = direction;
        d2 = 1;
        idx1 = i_idx_;
        idx2 = j_idx_;
    else
        d1 = 1;
        d2 = direction;
        idx1 = j_idx_;
        idx2 = i_idx_;
        
        centerDistance = abs(centerDistance);
    end
end
